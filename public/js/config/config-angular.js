/**
 * Created by tales on 26/12/15.
 */

(function(){
	App.config(function($interpolateProvider) {
		$interpolateProvider.startSymbol('{/');
		$interpolateProvider.endSymbol('/}');
	});
})()
