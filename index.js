var express = require('express');
var engines = require('consolidate');
var path = require('path');
var app = express();

app.set('port', (process.env.PORT || 5000));

app.use(express.static(path.join(__dirname + '/public')));

// views is directory for all template files
app.set('views', __dirname + '/views');

app.engine('html', engines.mustache);

app.set('view engine', 'html');


app.get('/', function(request, response) {
  response.render('pages/application');
});

app.get('/page', function(request, response) {
	response.render('pages/page');
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});


